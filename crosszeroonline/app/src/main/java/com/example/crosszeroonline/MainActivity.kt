package com.example.crosszeroonline

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.method.LinkMovementMethod
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener {
    private var id: String? = null
    private val ID = "ID"
    private val root = FirebaseDatabase.getInstance().reference.root
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        swipe_container.setOnRefreshListener(this)
        swipe_container.setColorSchemeResources(android.R.color.black,
            R.color.orange,
            android.R.color.black,
            R.color.orange)
        auth = FirebaseAuth.getInstance()
        id = auth.uid
        if (id == null) {
            registration()
        }
        //generate()
        getStat()
        play.setOnClickListener {
            val intent = Intent(this@MainActivity, ActivityPlay::class.java)
            intent.putExtra(ID, id)
            startActivity(intent)
        }
        rules.setOnClickListener {
            val aboutDialog: AlertDialog = AlertDialog.Builder(
                this@MainActivity
            ).setMessage("Это классические крестики-нолики с компьютером, вы играете за крестики.")
                .setPositiveButton("Понятно") { _, _ ->
                }.create()

            aboutDialog.show()

            (aboutDialog.findViewById(android.R.id.message) as TextView).movementMethod =
                LinkMovementMethod.getInstance()
        }
    }

    private fun getStat() {
        root.child("users")
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    stat.text = "0"
                }

                override fun onDataChange(p0: DataSnapshot) {
                    val map = p0.value as Map<String, Map<String, Int>>
                    if (map.keys.contains(id)) {
                        win.text = map[id]!!["win"].toString()
                        draw.text = map[id]!!["draw"].toString()
                        lose.text = map[id]!!["lose"].toString()
                    } else {
                        getStat()
                    }
                }

            })
    }

    private fun generate() {
        for (i in 0 until 512) {
            var br = false
            for (j in 0 until 3) {
                for (k in 0 until 3) {
                    if (i and (1 shl (j + k * 3)) == 0) {
                        root.child("motion").child(i.toString()).setValue(j + k * 3)
                        br = true
                        break
                    }
                }
                if (br) {
                    break
                }
            }
        }
    }

    private fun registration() {
        auth.signInAnonymously()
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    id = auth.uid.toString()
                    root.child("users").child(id!!).child("win").setValue(0)
                    root.child("users").child(id!!).child("draw").setValue(0)
                    root.child("users").child(id!!).child("lose").setValue(0)
                }
            }
    }

   override fun onRefresh() {
        Handler().postDelayed({
            swipe_container.isRefreshing = false
            getStat()
        }, 4000)
    }
}
