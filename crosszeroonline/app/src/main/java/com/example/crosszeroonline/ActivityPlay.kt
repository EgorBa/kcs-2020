package com.example.crosszeroonline

import android.app.AlertDialog
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_play.*


class ActivityPlay : AppCompatActivity() {

    private var game = Array(3) { Array(3) { 0 } }
    private var curPos = 0
    private var id: String? = null
    private val ID: String = "ID"
    private lateinit var allCross: Array<ImageView>
    private lateinit var allZero: Array<ImageView>
    private lateinit var allWinLine: Array<ImageView>
    private val root = FirebaseDatabase.getInstance().reference.root

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        id = intent.getStringExtra(ID)
        setContentView(R.layout.activity_play)
        allCross =
            arrayOf(LUcross, LMcross, LDcross, MUcross, MMcross, MDcross, RUcross, RMcross, RDcross)
        allZero = arrayOf(LUzero, LMzero, LDzero, MUzero, MMzero, MDzero, RUzero, RMzero, RDzero)
        allWinLine =
            arrayOf(winLine1, winLine2, winLine3, winLine4, winLine5, winLine6, winLine7, winLine8)
        clearAll()
        submit.setOnClickListener {
            val posStr: Int = curPos % 3
            val posCol: Int = curPos / 3
            game[posStr][posCol] = 1
            allCross[curPos].isClickable = false
            if (check(1)) {
                win()
            } else {
                if (endGame()) {
                    draw()
                } else {
                    send()
                    submit.isClickable = false
                }
            }
        }
        for (i in 0 until 9) {
            allCross[i].setOnClickListener {
                clearAllNotActiveCross()
                allCross[i].setImageResource(R.drawable.cross)
                curPos = i
                submit.isClickable = true
            }
        }
    }

    private fun clearAll() {
        title1.text = "Игра"
        submit.isClickable = false
        game = Array(3) { Array(3) { 0 } }

        for (c in allCross) {
            c.setImageResource(R.drawable.empty_cross)
            c.isClickable = true
        }

        for (line in allWinLine) {
            line.visibility = View.INVISIBLE
        }

        for (z in allZero) {
            z.visibility = View.INVISIBLE
        }
    }

    private fun clearAllNotActiveCross() {
        for (i in 0 until 9) {
            val posStr: Int = i % 3
            val posCol: Int = i / 3
            if (game[posStr][posCol] == 0) {
                allCross[i].setImageResource(R.drawable.empty_cross)
            }
        }
    }

    private fun check(a: Int): Boolean {
        if (game[0][0] + game[0][1] + game[0][2] == 3 * a) {
            winLine4.visibility = View.VISIBLE
            return true
        }
        if (game[1][0] + game[1][1] + game[1][2] == 3 * a) {
            winLine5.visibility = View.VISIBLE
            return true
        }
        if (game[2][0] + game[2][1] + game[2][2] == 3 * a) {
            winLine6.visibility = View.VISIBLE
            return true
        }
        if (game[0][0] + game[1][0] + game[2][0] == 3 * a) {
            winLine1.visibility = View.VISIBLE
            return true
        }
        if (game[0][1] + game[1][1] + game[2][1] == 3 * a) {
            winLine2.visibility = View.VISIBLE
            return true
        }
        if (game[0][2] + game[1][2] + game[2][2] == 3 * a) {
            winLine3.visibility = View.VISIBLE
            return true
        }
        if (game[0][0] + game[1][1] + game[2][2] == 3 * a) {
            winLine7.visibility = View.VISIBLE
            return true
        }
        if (game[0][2] + game[1][1] + game[2][0] == 3 * a) {
            winLine8.visibility = View.VISIBLE
            return true
        }
        return false
    }

    private fun send() {
        val cross = numberValue(1)
        val zero = numberValue(-1)
        val key = zero + cross
        root.child("motion")
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                }

                override fun onDataChange(p0: DataSnapshot) {
                    val map = p0.value as List<Int>
                    val index = map[key]
                    val i = index % 3
                    val j = index / 3
                    game[i][j] = -1
                    if (check(-1)) {
                        lose()
                    }
                    putZeroOn(i + j * 3)
                    System.out.printf("a : %s\n",index)
                }

            })
    }

    private fun numberValue(a: Int): Int{
        var sum = 0
        for (i in 0 until 3) {
            for (j in 0 until 3) {
                if (game[i][j] == a) {
                    sum += 1 shl (i + j * 3)
                }
            }
        }
        return sum
    }

    private fun lose() {
        title1.text = "Ты проиграл"
        showAlert()
        set("lose")
    }

    private fun win() {
        title1.text = "Ты выиграл"
        showAlert()
        set("win")
    }

    private fun draw() {
        title1.text = "Ничья"
        showAlert()
        set("draw")
    }

    private fun endGame(): Boolean {
        for (i in 0 until 3) {
            for (j in 0 until 3) {
                if (game[i][j] == 0) {
                    return false
                }
            }
        }
        return true
    }

    private fun showAlert() {
        val aboutDialog: AlertDialog = AlertDialog.Builder(
            this@ActivityPlay
        ).setMessage("Сыграем еще?")
            .setNegativeButton("Нее") { _, _ ->
                clearAll()
                this@ActivityPlay.finish()
            }
            .setPositiveButton("Даа") { _, _ ->
                clearAll()
            }.create()

        aboutDialog.show()
        aboutDialog.setCanceledOnTouchOutside(false)
        aboutDialog.setCancelable(false)

        (aboutDialog.findViewById(android.R.id.message) as TextView).movementMethod =
            LinkMovementMethod.getInstance()
    }

    private fun putZeroOn(a: Int) {
        allCross[a].isClickable = false
        allZero[a].visibility = View.VISIBLE
    }

    private fun set(str: String) {
        root.child("users")
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                }

                override fun onDataChange(p0: DataSnapshot) {
                    val map = p0.value as Map<String, Map<String, Int>>
                    val value : Int = map[id]!!.getValue(str)
                    root.child("users").child(id!!).child(str).setValue(value + 1)
                }

            })
    }

}
